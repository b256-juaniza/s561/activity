function countLetter(letter, sentence) {
    let ct = 0;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    if (letter.length === 1) {
        for (let i = sentence.length; i >= 0; i--) {
            if(sentence[i] === letter) {
                ct++;
            }
        }
        return ct;
    // If letter is invalid, return undefined.
    } else {
        return undefined;
    }
}

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    let arr = [];
    let a = true;
    for (var i = text.length - 1; i >= 0; i--) {
        arr.push(text[i]);
    }
    arr.sort();
    for (var i = arr.length - 1; i >= 0; i--) {
        if(arr[i] == arr[i+1]) {
            a = false;
        }
    }
    return a;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    let x = 0;
    if (age < 13) {
         x = undefined;
    } else if (age >= 13 && age <= 21 || age > 64){
         x = (price - (price * 0.2));
    } else if (age > 21 && age <= 64){
         x = (price);
    }
    let y = x;

    if (y != undefined) {
        y = x.toFixed(2);
        y.toString(); 
    }

    return y;
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    let arr = [];

    for (var i = 0; i < items.length - 1; i++) {
        if(items[i].stocks === 0){
            if (arr.includes(items[i].category)) {
               arr.splice(items[i].category);
            }
            arr.push(items[i].category);
        }
    }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    return arr;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let arr = [];
    for (var i = candidateB.length - 1; i >= 0; i--) {
        if(candidateA.includes(candidateB[i])){
            if (arr.includes(candidateB[i])) {
                arr.splice(candidateB[i])
            }
            arr.push(candidateB[i]);
        }
    }
    return arr;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};